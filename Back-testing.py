
# coding: utf-8

# In[1]:


#import sys


# In[2]:


#sys.path.insert(1, "/home/govind/trading-infra")
#from strategy4 import Strategy4


# In[3]:


import numpy as np
from datetime import datetime
import pandas as pd
#pd.options.mode.chained_assignment = None  # default='warn'

ENTRY_RATIO = 0.01
RE_ENTRY_RATIO = 0.01
EXIT_RATIO = 0.03
TOTAL_INVESTMENT = 1000
TRADING_FEES = 0.004


class Strategy4(object):

    def __init__(self):
        super(Strategy4, self).__init__()
        print('Strategy 4 running...')

    def preprocess_data(self, data):
        # data['timestamp'] = data['timestamp'].apply(
        #     lambda x: datetime.fromtimestamp(x / 1000).strftime('%Y-%m-%d %H:%M:%S'))
        data = data.sort_values(['timestamp'], ascending=True)
        data = data.reset_index(drop=True)
        data.volume = data.volume.apply(lambda x: int(x * 1000))
        data.open = data.open.apply(lambda x: round(x, 2))
        data.close = data.close.apply(lambda x: round(x, 2))
        data.high = data.high.apply(lambda x: round(x, 2))
        data.low = data.low.apply(lambda x: round(x, 2))
        data['rolling_max'] = data.loc[:, ['open', 'close']].apply(np.max, axis=1)
        rf = data['rolling_max'].rolling(window=5, min_periods=1)
        data['rolling_max'] = rf.max()
        return data

    class StrategyConfig(object):
        PROFIT_MARGIN = 0.05
        PREV_SELL_BUY_MARGIN = 0.001
        MIN_STABLE_PERIOD = 1
        ABSOLUTE_ENTRANT_PRICE = -1
        ABSOLUTE_EXIT_PRICE = -1
        MAX_SINGLE_TRADE_LIMIT = 270

    # Method to check exit condition in case of loss or profit
    def check_exit_logic(self, new_candle, trade_df):
        num_trades = len(trade_df)
        if num_trades > 0 and trade_df['is_trade_open'][num_trades - 1]==True:
            df = trade_df.iloc[num_trades - 1]
            wtd_entry_price = df['wtd_entry_price']
            num_iterations = int(df['iterations'])

            # Condition for exiting trade with profit
            if (new_candle.close-wtd_entry_price)/wtd_entry_price >= EXIT_RATIO:
                exit_timestamp = new_candle.timestamp
                exit_price = new_candle.close
                for i in range(num_iterations):
                    trade_df['is_trade_open'][num_trades-1-i] = False
                    trade_df['exit_price'][num_trades-1-i] = exit_price
                    trade_df['exit_timestamp'][num_trades-1-i] = exit_timestamp
                    trade_df['net_balance'][num_trades-1-i] =                         df['net_balance'] + df['total_btc_traded']*exit_price
                print("%s BTC sold at %s USD at %s" % (df['total_btc_traded'],
                                                       exit_price, exit_timestamp))

            # Condition for exiting trade with loss
            if (df['iterations'] == 5) and (df['entry_price']-new_candle.close)/df['entry_price'] >= RE_ENTRY_RATIO:
                exit_timestamp = new_candle.timestamp
                exit_price = new_candle.close
                for i in range(num_iterations):
                    trade_df['is_trade_open'][num_trades-1-i] = False
                    trade_df['exit_price'][num_trades-1-i] = exit_price
                    trade_df['exit_timestamp'][num_trades-1-i] = exit_timestamp
                    trade_df['net_balance'][num_trades - 1 - i] =                         df['net_balance'] + df['total_btc_traded'] * exit_price
                print("%s BTC sold at %s USD at %s" % (df['total_btc_traded'],
                                                       exit_price, exit_timestamp))
        return trade_df

    # Method to check new trade entry condition
    def check_entry_logic(self, candle_df, new_candle, trade_df):
        num_trades = len(trade_df)
        if num_trades == 0:
            local_max_price = candle_df['rolling_max'][len(candle_df)-1]
            if (local_max_price-new_candle.close)/local_max_price >= ENTRY_RATIO:
                entry_timestamp = new_candle['timestamp']
                entry_price = new_candle['close']
                wtd_entry_price = entry_price
                num_btc_traded = 0.5*(TOTAL_INVESTMENT/new_candle['close'])*(1-TRADING_FEES)
                total_btc_traded = num_btc_traded
                trading_fees_paid = entry_price * num_btc_traded * TRADING_FEES
                net_balance = TOTAL_INVESTMENT - entry_price*num_btc_traded - trading_fees_paid
                is_trade_open = True
                iterations = 1
                row = [entry_timestamp, '', entry_price, wtd_entry_price, 0, num_btc_traded,
                       total_btc_traded, trading_fees_paid, iterations, is_trade_open, net_balance]
                trade_df.loc[num_trades] = row
                print("%s BTC bought at %s USD at %s" % (num_btc_traded, new_candle.close, new_candle.timestamp))
            #return trade_df

        elif not trade_df['is_trade_open'][num_trades-1]:
            local_max_price = candle_df['rolling_max'][len(candle_df)-1]
            if ((local_max_price - new_candle.close)/local_max_price) >= ENTRY_RATIO:
                entry_timestamp = new_candle['timestamp']
                entry_price = new_candle['close']
                num_btc_traded = 0.5*((trade_df['net_balance'][num_trades-1]) /
                                      new_candle['close'])*(1-TRADING_FEES)
                total_btc_traded = num_btc_traded
                wtd_entry_price = entry_price
                trading_fees_paid = entry_price * num_btc_traded * TRADING_FEES
                net_balance = (trade_df['net_balance'][num_trades-1]) -                               entry_price*num_btc_traded - trading_fees_paid
                is_trade_open = True
                iterations = 1
                row = [entry_timestamp, '', entry_price, wtd_entry_price, 0, num_btc_traded,
                       total_btc_traded, trading_fees_paid, iterations, is_trade_open, net_balance]
                trade_df.loc[num_trades] = row
                print("%s BTC bought at %s USD at %s" % (num_btc_traded, new_candle.close, new_candle.timestamp))
        return trade_df

    def check_reentry_logic(self, new_candle, trade_df):
        num_trades = len(trade_df)
        if num_trades > 0 and trade_df['is_trade_open'][num_trades-1]==True and                         trade_df['iterations'][num_trades-1] < 5:
            df = trade_df.iloc[num_trades-1]
            if (df['entry_price']-new_candle.close)/df['entry_price'] > RE_ENTRY_RATIO:
                num_iterations = df['iterations']
                entry_timestamp = new_candle['timestamp']
                entry_price = new_candle['close']
                num_btc_traded = 0.5 * df['num_btc_traded'] * (1 - TRADING_FEES)
                total_btc_traded = df['total_btc_traded'] + num_btc_traded
                wtd_entry_price = (df['wtd_entry_price'] * df['total_btc_traded'] +
                                   new_candle['close'] * num_btc_traded) / total_btc_traded
                trading_fees_paid = entry_price * num_btc_traded * TRADING_FEES
                net_balance = (trade_df['net_balance'][num_trades - 1]) -                               entry_price * num_btc_traded - trading_fees_paid
                is_trade_open = True
                iterations = num_iterations + 1
                row = [entry_timestamp, '', entry_price, wtd_entry_price, 0, num_btc_traded,
                       total_btc_traded, trading_fees_paid, iterations, is_trade_open, net_balance]
                trade_df.loc[num_trades] = row
                print("%s BTC bought at %s USD at %s" % (num_btc_traded, new_candle.close, new_candle.timestamp))
        return trade_df

    def run_strategy(self, candle_df, i, j, trade_df):
        new_candle = candle_df.iloc[j]
#         new_candle['timestamp'] = datetime.fromtimestamp(
#             new_candle['timestamp']/1000).strftime('%Y-%m-%d %H:%M:%S')
        new_candle.volume = new_candle.volume*1000
        new_candle.open = round(new_candle.open, 2)
        new_candle.close = round(new_candle.close, 2)
        new_candle.high = round(new_candle.high, 2)
        new_candle.low = round(new_candle.low, 2)
        total_candles_df = candle_df[i:j]
        total_candles_df = self.preprocess_data(total_candles_df)
        trade_df = self.check_entry_logic(total_candles_df, new_candle, trade_df)
        trade_df = self.check_reentry_logic(new_candle, trade_df)
        trade_df = self.check_exit_logic(new_candle, trade_df)
        if len(trade_df)>0 and trade_df['is_trade_open'][len(trade_df)-1]==False:
            i=j
        j=j+1
        return trade_df, i, j


# In[14]:


import pandas as pd
from datetime import datetime
data = pd.read_csv('btccandles.csv')
data = data[['timestamp','open', 'close','high', 'low', 'volume']]
data = data.drop_duplicates(subset=['timestamp'])
# data['timestamp'] = data['timestamp'].apply(
#             lambda x: datetime.fromtimestamp(x / 1000).strftime('%Y-%m-%d %H:%M:%S'))
data = data.sort_values(by=['timestamp'])
data.timestamp = pd.to_datetime(data.timestamp)
#data = data[data['timestamp']>'2017-10-01']
data = data.reset_index(drop=True)


# In[7]:


i=0
j=1
trade_df = pd.DataFrame(columns=['entry_timestamp', 'exit_timestamp', 'entry_price',
                                         'wtd_entry_price', 'exit_price', 'num_btc_traded',
                                         'total_btc_traded', 'trading_fees_paid',
                                         'iterations', 'is_trade_open', 'net_balance'])


# In[3]:





# In[8]:


for k in range(len(data)-2):
    print("Running loop %s"%k)
    b = Strategy4()
    trade_df, i , j = b.run_strategy(data, i, j, trade_df)
    if len(trade_df)>0:
        trade_df.to_csv('btc_backtest_window_5_entry_0.01_exit_0.03.csv', index=False)


# In[1]:


#data


# In[ ]:




